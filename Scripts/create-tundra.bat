@echo off
title Tundra Helper
echo Press any key to download and integrate Tundra
echo Make sure that you have all the dependencies!
pause>nul
cls
echo Initializing the local repo
git init

echo.
echo Creating the required directories
mkdir Assets
cd Assets
mkdir Maps
mkdir Tundra

echo.
echo Downloading all the submodules
cd Tundra
git submodule add git@gitlab.com:PITR_DEV/tundra.core.git Core
git submodule add git@gitlab.com:PITR_DEV/tundra.workshop.git Workshop
git submodule add git@gitlab.com:PITR_DEV/tundra.components.git Components
cd ..
git submodule add git@gitlab.com:PITR_DEV/tundra.prefabs.git "Tundra Prefabs"

echo.
echo Brace yourself, downloading (a big) ULTRAKILL content pack
git submodule add git@gitlab.com:PITR_DEV/tundra.assets.git Common

cd ..

echo.
echo Downloading the project config
git clone git@gitlab.com:PITR_DEV/tundra.config.git TempTundraConfig

echo.
echo Applying general configs
xcopy /Y /Q /R TempTundraConfig\Git\.gitignore .gitignore*
xcopy /Y /Q /R TempTundraConfig\Git\.gitattributes .gitattributes*

echo.
echo Applying the project settings
xcopy /Y /Q /R TempTundraConfig\ProjectSettings\EditorSettings.asset ProjectSettings\EditorSettings.asset
xcopy /Y /Q /R TempTundraConfig\ProjectSettings\ProjectVersion.txt ProjectSettings\ProjectVersion.txt
xcopy /Y /Q /R TempTundraConfig\ProjectSettings\TagManager.asset ProjectSettings\TagManager.asset
xcopy /Y /Q /R TempTundraConfig\ProjectSettings\TimeManager.asset ProjectSettings\TimeManager.asset

echo.
echo Settings the project packages
xcopy /Y /Q /R TempTundraConfig\Packages\manifest.json Packages\manifest.json
xcopy /Y /Q /R TempTundraConfig\Packages\packages-lock.json Packages\packages-lock.json

echo.
echo Removing the temporary config directory
rmdir /Q /S TempTundraConfig

echo.
echo Hopefully everything went well.
echo Hopefully.
pause