@echo off
title Tundra Helper
cd Assets
cd Tundra

cd Core
git reset --hard
git clean --force -d -x
git checkout master
git pull
cd ..

cd Workshop
git reset --hard
git clean --force -d -x
git checkout master
git pull
cd ..

cd Components
git reset --hard
git clean --force -d -x
git checkout master
git pull
cd ..

cd ..

cd "Tundra Prefabs"
git reset --hard
git clean --force -d -x
git checkout master
git pull
cd ..

cd Common
git reset --hard
git clean --force -d -x
git checkout master
git pull
cd ..

echo Done
pause
